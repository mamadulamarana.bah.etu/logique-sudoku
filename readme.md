# Résolution du sudoku avec python-sat

Ce TP consiste à implémenter un solveur de sudoku à l'aide d'un solveur SAT. Il
reprend les étapes de la feuille de
[TD2](https://www.fil.univ-lille1.fr/~salvati/cours/logique/content/sat_reduction_enonce.pdf)
Comme vous pourrez le constater cela nécessite d'écrire relativement peu de
code. Néanmoins, le modèle logique employé doit être correct pour que le solveur
de sudoku fonctionne.

## Intallation de python-sat

### Linux et MacOS

Pour faire le TP, il faut que vous installiez le module python-sat sur votre
compte. Pour cela tapez dans une console shell la commande suivante :

```shell
pip3 install python-sat
```

### Windows

  1) Activez le WSL en allant dans le menu démarrer et en choisissant Activer ou Désactiver des fonctionnalités de windows et en cochant Sous système Windows pour Linux. Redémarrez.
  2) Téléchargez ubuntu : https://aka.ms/wslubuntu2004 et installez-la. Lors de l'installation vous devrez choisir un nom d'utilisateur et un mot de passe.
  3) Dans le shell, tapez :
     ```shell
     sudo apt update
     ```
  4) puis
     ```shell
     sudo apt upgrade
     ```
     
  5) puis
     ```shell
     sudo apt install python3-pip
     ```
  6) puis enfin
     ```shell
     pip3 install python-sat
     ```
  7) supposons que vous ayez sauvegardé le fichier `australie.py` ici  :
     `d:\logique\australie.py`
  8) testez
     ```shell
     python3 /mnt/d/logique/australie.py
     ```
  9) Pour les TPs, vous devrez travailler depuis le WSL.


## Instructions pour le TP

1. Veuillez /forker/ le dépôt indiqué par votre chargé de TD.
2. Invitez votre chargé de TD comme développeur dans votre projet.
3. Suivez les indications contenues dans le fichier sudoku.py.
